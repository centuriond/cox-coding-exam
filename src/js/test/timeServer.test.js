const timeService = require('../main/AppointmentListService');

createDate = (hour) => {
	return new Date().setHours(hour, 0, 0, 0);
}


describe('Test Time Generating Service', () => {
	
	it('will generate dates from 7 to 9', () => {
		
		let expected = [createDate(7), createDate(8), createDate(9)];
		let actual = timeService.generate(7, 3);

		expect(actual).toEqual(expected);
	});

	
	it('will return [] if negative number of hours is a parameter', () => {
		
		expect(timeService.generate(10, -5)).toEqual([]);

	});

	it('will return [] if a negative starting hour is used', () => {
		
		expect(() => { timeService.generate(-5, 5); }).toThrow('Valid range for starting hour is 0 to 23');

	});

	it('will return [] if an hour larger than 23 is used (valid range is 0-23)', () => {
		
		expect(() => { timeService.generate(-5, 5); }).toThrow('Valid range for starting hour is 0 to 23');

	});
});