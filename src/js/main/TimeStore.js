'use strict';

import { createStore } from 'redux'
import TimeStoreReducers from './reducers'

let store = createStore(TimeStoreReducers);

module.exports = store;