
document.addEventListener("DOMContentLoaded", (event) => {
	let React = require('react');
	let ReactDOM = require('react-dom');
	let TimeApplication = require('./TimeApplication');
	
	ReactDOM.render(<TimeApplication />, document.getElementById('application'));
});
