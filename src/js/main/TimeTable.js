'use strict';

let React = require('react');
let TimeRow = require('./TimeRow');
let TimeStore = require('./TimeStore');

class TimeTable extends React.Component {
	
	render() {
		let rows = [];
		let times = Object.keys(TimeStore.getState().times);
		let storeData = TimeStore.getState().times;

		times.forEach(time => {
			rows.push(
				<TimeRow time = {time} key = {time} />
			);
		});

		return (<div>{rows}</div>);
	}
}

module.exports = TimeTable;