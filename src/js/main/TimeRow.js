'use strict';

const React = require('react');
const TimeStore = require('./TimeStore');
const TimeModal = require('./TimeModal');

class TimeRow extends React.Component {

	constructor (props) {
		super(props);
		this.state = {};
		this.state.isModalOpen = false;
		let state = TimeStore.getState().times[this.props.time];
		this.state.isBooked = state.isBooked;
	}

	openModal() {
		this.setState({ 
			isModalOpen: true
		});
	}

	closeModal() {
		let name = document.getElementById('modal-name-id').value;
		let phone = document.getElementById('modal-phone-id').value;
		
		TimeStore.dispatch({
			type: 'BOOK_TIME',
			time: this.props.time,
			isBooked: name && phone,
			name: name,
			phone: phone
		});

		// close modal
		this.setState({
			isModalOpen: false,
			isBooked: name && phone
		});
	}

	render () {
		let timeMs = Number.parseInt(this.props.time)
		let dateStr = new Date(timeMs).toLocaleTimeString();
		let classes = "timeRow";
		if (this.state.isBooked) {
			classes += " booked";
		}

	    return (
	    	<div className={classes} onClick={() => this.openModal()}>
	    		{dateStr}
		    	<TimeModal 
		    		isModalOpen = {this.state.isModalOpen}
		    		time = {timeMs}
		    		dateStr = {dateStr}
		    		onClose = {() => { this.closeModal() }}
		    	/>
          	</div>
	    );
	}
}

module.exports = TimeRow;
