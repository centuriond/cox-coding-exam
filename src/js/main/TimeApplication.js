'use strict';

const React = require('react');
const TimeTable = require('./TimeTable');

class TimeApplication extends React.Component {
	constructor (props) {
		super(props);
	}

	componentDidMount () {
		// TimeStore.addChangeListener(() => {
		// 	this.setState(getTimeState());
		// });
	}

	componentWillUnmount () {
   //  	TimeStore.removeChangeListener(() => {
			// this.setState(getTimeState());
   //  	});
  	}

	render () {
		return <TimeTable />
	}

}

module.exports = TimeApplication;
