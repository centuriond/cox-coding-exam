'use strict';

let React = require('react');
let Modal = require('react-modal');
let TimeStore = require('./TimeStore');

class TimeModal extends React.Component {

	constructor (props) {
		super(props);
		this.state = {};
		this.state.isModalOpen = false;
	}

	afterOpenModal () {
		/* load data from the store */
		let data = TimeStore.getState().times[this.time];
		
		/* set the input fields here */
		if (data) {
			document.getElementById('modal-name-id').value = data.name || '';
			document.getElementById('modal-phone-id').value = data.phone || '';
		}
	}

	render () {
		let isModalOpen = this.props.isModalOpen;
		this.state.isModalOpen = isModalOpen;

		return (
			<Modal
				isOpen = {this.state.isModalOpen}
				onAfterOpen = {this.afterOpenModal}
				time = {this.props.time}
				onClose = {this.props.onClose}
				className = "dlg-modal"
			>
				<h2 className="dlg-header">Appointment for {this.props.dateStr}</h2>

				<div className="inputRow">
					Name:
					<input 
						type="text" 
						name="name"
						className="dlg-input"
						id="modal-name-id"/>
				</div> 

				<div className="inputRow">
					Phone:
					<input 
						type="text"
						className="dlg-input"
						name="phone"
						id="modal-phone-id"/>
				</div>

				<div className="dlg-save">
					<button className="dlg-save" onClick={this.props.onClose}>Close</button>
				</div>

			</Modal>
		);
	}

}

module.exports = TimeModal;
