'use strict';

import { combineReducers } from 'redux';

const assign = require('object-assign');
const TimeService = require('./AppointmentListService');

const START_HOUR = 9; /* 9am */
const NUM_HOURS = 10; /* hours worth of data to generate */


const initialTimes = TimeService.generate(START_HOUR, NUM_HOURS);
const initialState = {};

/* create the initialState */

initialTimes.forEach(time => {
	initialState[time] = {
		isBooked: false,
		name: '',
		phone: ''
	};
});

/* define the time reducer */
const times = (state = initialState, action) => {

	switch(action.type) {
		case 'BOOK_TIME':
			/* create NEW state object */
			let newState = assign({}, state);
			newState[action.time] = {
				isBooked: action.isBooked,
				phone: action.phone,
				name: action.name
			};

			return newState;

		default:
			return state;
	}
}

const TimeStoreReducers = combineReducers({
	times
});

export default TimeStoreReducers;
