'use strict';

let AppointmentListService = {
	generate: (startingHour, numberOfHours) => {
		let result = [];

		if (startingHour < 0 || startingHour > 23) {
			throw 'Valid range for starting hour is 0 to 23';
		}

		for (let i = 0; i < numberOfHours; i += 1 ) {
			result.push(
				new Date().setHours(startingHour + i, 0, 0, 0)
			);
		}

		return result;
	}
};

module.exports = AppointmentListService;