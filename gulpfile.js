const gulp = require('gulp');
const babel = require('gulp-babel');
const browserify = require('browserify');
const source = require('vinyl-source-stream');
const del = require('del');
const uglify = require('gulp-uglify');
var jasmine = require('gulp-jasmine');


/* copying tasks */
gulp.task('copyHtmlFiles', () => {
	return gulp.src('src/*.html')
		.pipe(gulp.dest('build'));
});

gulp.task('copyCssFiles', () => {
	return gulp.src('src/css/*.css')
		.pipe(gulp.dest('build/css'));
});

gulp.task('copyServer', () => {
	return gulp.src('server.js')
		.pipe(gulp.dest('build'));
});


/* jasmine tests */
gulp.task('test', () => {
	return gulp.src(['src/js/test/**/*.js'])
			.pipe(jasmine());
});


/* transpiling stuff */
gulp.task('es6', ['test', 'copyCssFiles', 'copyHtmlFiles'], () => {
	return gulp.src('src/js/main/**/*.js')
			.pipe(babel({
				presets: ['es2015', 'react']
			}))
			.pipe(gulp.dest('build'));
});


/* bundle and compress everything together */
gulp.task('bundle' ,['es6'], () => {
	return browserify('build/home.js')
		.bundle()
		.pipe(source('bundle.js'))
        .pipe(gulp.dest('build/js/'));
});


/* used only really to debug things (really just avoids minifications and such)*/
gulp.task('debugBuild', ['bundle', 'copyServer'], () => {
	return del(['build/**/*.js', '!build/server.js', '!build/js/bundle.js']);
});


/* final compression and such */
gulp.task('build', ['debugBuild'], () => {
	return gulp.src(['build/js/bundle.js'])
		.pipe(uglify())
		.pipe(gulp.dest('build/js'));
});

gulp.task('default', ['build']);
