Simple scheduling application using NodeJs and React using the Flux pattern.

- Built using NodeJs v7.2.0

Running notes:

- Have NodeJs/npm installed
- clone the repo
- navigate to the location of the repo on your machine in your terminal
- type the following to build the applciation: npm install && gulp build
  - Note: npm may give an error about file permissions.
  - If this happens, run the following command in your terminal: sudo npm install && gulp build
- Navigate to the build directory and type in your terminal: node server.js
- open Web broswer to the address (default address is http://localhost:8082)
