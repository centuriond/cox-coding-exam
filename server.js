'use strict';
/* Used as the server startup */

/* basic server config */
const port = 8082;
const serverUrl = 'http://localhost:' + port;

/* dependencies */
let http = require("http");
let fs = require('fs');
let url = require('url');

const server = http.createServer((request, response) => {

	let fileToLoad, 
		urlPath = url.parse(request.url).pathname;

	fileToLoad = urlPath.substr(1);
	if (fileToLoad === '') {
		fileToLoad = 'index.html';
	}

	console.log("Request for " + fileToLoad + " received.");

	fs.readFile(fileToLoad, (err, data) => {

		if (err) {
			console.log(err);
			response.writeHead(404, {'Content-Type': 'text/html'});
		} else {
			let mimeType = 'html';

			if (fileToLoad.indexOf('.css') > -1) {
				mimeType = 'text/css';	
			}

			response.writeHead(200, {'Content-Type': mimeType});	
			response.write(data.toString());		
		}

		response.end();

	});

}).listen(port);

console.log('Server startup: ' + serverUrl);